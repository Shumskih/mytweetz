# MyTweetz

This project using twitter API. It allows us two things: to display tweets along with likes and retweets as well as create a tweet. User able to upload images.

##### Used technologies:

- Laravel PHP Framework
- Bootstrap CSS Framework
- [thujohn/twitter](https://github.com/thujohn/twitter) - Twitter API for Laravel 4/5